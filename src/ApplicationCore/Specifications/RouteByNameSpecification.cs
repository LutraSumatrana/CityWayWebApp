﻿using System.Linq;
using Ardalis.Specification;
using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public sealed class RouteByNameSpecification : Specification<Route>
    {
        public RouteByNameSpecification(string name)
        {
            Query
                .Where(r => r.RouteName == name)
                .Include(r => r.RoutePoints);
        }
    }
}
