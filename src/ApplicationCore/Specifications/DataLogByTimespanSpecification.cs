﻿using Ardalis.Specification;
using ApplicationCore.Entities;
using System;

namespace ApplicationCore.Specifications
{
    public class DataLogByTimespanSpecification : Specification<DataLog>
    {
        public DataLogByTimespanSpecification(TimeSpan begin, TimeSpan end)
        {
            Query.Where(dl => (dl.DataTime >= begin && dl.DataTime <= end));
        }
    }
}
