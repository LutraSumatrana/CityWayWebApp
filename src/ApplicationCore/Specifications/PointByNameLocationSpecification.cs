﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ardalis.Specification;
using ApplicationCore.Entities;
//using NetTopologySuite.Geometries;
using ApplicationCore.EntitiesApp;


namespace ApplicationCore.Specifications
{
    class PointByNameLocationSpecification : Specification<Point>
    {
       
      //  public PointByNameLocationSpecification(string _pointName, NetTopologySuite.Geometries.Geometry _geoLocation)
      //  {
      //      Query.Where(r => r.PointName == _pointName && r.PointLocation.EqualsTopologically(_geoLocation));
      //  }

        public PointByNameLocationSpecification(string _pointName, GeoLocation _geoLocation)
        {
            Query.Where(r => r.PointName == _pointName && r.PointLocation.EqualsTopologically(_geoLocation));
        }
    }



}
