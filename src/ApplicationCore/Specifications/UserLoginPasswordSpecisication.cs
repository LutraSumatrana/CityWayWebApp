﻿using System.Linq;
using Ardalis.Specification;
using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class UserLoginPasswordSpecisication : Specification<User>
    {
        public UserLoginPasswordSpecisication(string login, string password)
        {
            Query.Where((u => u.UserLogin == login && u.UserPassword == password));
        }
    }
}
