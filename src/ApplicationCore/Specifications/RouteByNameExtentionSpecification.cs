﻿using System.Linq;
using Ardalis.Specification;
using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class RouteByNameExtentionSpecification : Specification<Route>
    {
        public RouteByNameExtentionSpecification(string name, string extention)
        {
            Query.Where(r => (r.RouteName == name && r.RouteNameExtention == extention));

        }
    }
}
