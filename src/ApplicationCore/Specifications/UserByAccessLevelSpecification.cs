﻿using System.Linq;
using Ardalis.Specification;
using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class UserByAccessLevelSpecification :Specification<User>
    {
        public UserByAccessLevelSpecification(int level)
        {
            Query.Where(u => u.AccessLevel == level);
        }
    }
}
