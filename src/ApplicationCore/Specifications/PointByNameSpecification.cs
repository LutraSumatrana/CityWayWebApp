﻿using System.Linq;
using Ardalis.Specification;
using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class PointByNameSpecification : Specification<Point>
    {
        public PointByNameSpecification(string name)
        {
            Query.Where(r => r.PointName == name);
        }
    }
    
}
