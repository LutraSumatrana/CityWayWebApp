﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.Entities;
using Ardalis.Specification;

namespace ApplicationCore.Specifications
{
      public class TransportTypesBySpecification : Specification <TransportType>  // example for testing specialisation - tested
    {
        public TransportTypesBySpecification(List<string> transportypes)
        {
            Query.Where(r => transportypes.Contains(r.TransportTypeId));
          //  Query.Include(r => r.RoutePoints);
        }

    }
}
