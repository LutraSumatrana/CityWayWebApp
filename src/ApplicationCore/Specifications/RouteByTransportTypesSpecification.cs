﻿using System.Linq;
using Ardalis.Specification;
using ApplicationCore.Entities;
using System.Collections.Generic;


namespace ApplicationCore.Specifications
{
    public class RouteByTransportTypesSpecification : Specification<Route>
    {
        public RouteByTransportTypesSpecification(List<string> transportypes)
        {
            Query.Where(r => transportypes.Contains(r.TransportTypeId));
            Query.Include(r => r.RoutePoints);
        }

    }
}

   
