﻿using System.Linq;
using Ardalis.Specification;
using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class TimetableByRouteSpecification :Specification <Timetable>
    {
        public TimetableByRouteSpecification(int routeId)
        {
            Query.Where(tt => tt.RouteId == routeId);

        }
    }
}
