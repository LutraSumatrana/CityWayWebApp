﻿using System.Linq;
using Ardalis.Specification;
using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class RouteTimetableByTerminal :Specification <Route>
    {
        public RouteTimetableByTerminal(int pointId)
        {
            Query
                .Where(r => r.RouteTerminalStart == pointId)
                .Include(r => r.Timetables);
        }
    }
}
