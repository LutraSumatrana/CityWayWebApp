﻿using System.Linq;
using Ardalis.Specification;
using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{

    public class PointIsStopSpecification : Specification<Point>
    {
        public PointIsStopSpecification()
        {
            Query
            .Where(r => r.PointName != "");
            // .Where(r => (r.PointName != "" && r.isActive == 1));
        }

    }
}
