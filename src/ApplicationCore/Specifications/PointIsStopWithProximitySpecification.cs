﻿using Ardalis.Specification;
using ApplicationCore.Entities;
using System;

namespace ApplicationCore.Specifications
{
    public class PointIsStopWithProximitySpecification : Specification<Point>
    {
        public PointIsStopWithProximitySpecification(NetTopologySuite.Geometries.Geometry geoPoint, double proximity)
        {
            //  Query.Where(p => p.PointLocation.Distance(geoPoint)!=0.0);
            //Query.Where(p => geoPoint.Distance(p.PointLocation) <= proximity);
            //  Query.Where(p => p.PointLocation.Distance(geoPoint) <= proximity);
            
            Query.Where(p => p.PointLocation.IsWithinDistance(geoPoint, proximity) && p.PointName != "");

            // Query.Where(p => p.PointLocation.Distance(geoPoint) <= proximity && p.PointName != "");
            //  Query.Where(p => p.PointName != "");
        }
    }
}
