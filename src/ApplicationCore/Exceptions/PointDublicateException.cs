﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Exceptions
{
    public class PointDuplicateException : Exception
    {
       
        public PointDuplicateException(string _pointName, double _Longitude, double _Latitude) : base($"Already exists point with name {_pointName} and location {_Longitude};{_Latitude}")
        {
        }

        protected PointDuplicateException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }

        public PointDuplicateException(string message) : base(message)
        {
        }

        public PointDuplicateException(string message, Exception innerException) : base(message, innerException)
        {
        }

    }
}


