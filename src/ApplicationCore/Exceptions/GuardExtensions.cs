﻿using ApplicationCore.Exceptions;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.Entities;
//using Ardalis.GuardClauses;

namespace Ardalis.GuardClauses
{
    public static class DataGuards
    {
        public static void NullPoint(this IGuardClause guardClause, int _pointId, Point _point)
        {
            if (_point == null)
                throw new PointNotFoundException(_pointId);
        }

        public static void DublicatePoint(this IGuardClause guardClause, string _pointName, double  _Longitude, double _Latitude, Point _point)
        {
            if (_point != null)
                throw new PointDuplicateException(_pointName, _Longitude, _Latitude);
        }

        //public static void EmptyBasketOnCheckout(this IGuardClause guardClause, IReadOnlyCollection<BasketItem> basketItems)
        //{
        //    if (!basketItems.Any())
        //        throw new EmptyBasketOnCheckoutException();
        //}
    }
}