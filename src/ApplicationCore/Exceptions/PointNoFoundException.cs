﻿using System;

namespace ApplicationCore.Exceptions
{
    public class PointNotFoundException : Exception
    {
        public PointNotFoundException(int pointId) : base($"No point found with id {pointId}")
        {
        }

        protected PointNotFoundException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }

        public PointNotFoundException(string message) : base(message)
        {
        }

        public PointNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
