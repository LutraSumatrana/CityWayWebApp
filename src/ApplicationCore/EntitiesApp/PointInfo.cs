﻿using NetTopologySuite.Geometries;
using System.Collections.Generic;

namespace ApplicationCore.EntitiesApp
{
    //клас зупинок (можемо використовувати для посадки/висадки/пересадки/зміни маршрутів)
    public class PointInfo  // інформація про точки-зупинки
    {
        public PointInfo()
        {
            
        }

        public int PointId { get; set; }
        public string PointName { get; set; }
        public Geometry PointLocation { get; set; }

        public List<int> Routes { get; set; }  //перелік id маршрутів, що проходять через цю зупинку
    }
}
