﻿

namespace ApplicationCore.EntitiesApp
{

    public  class GeoLocation : NetTopologySuite.Geometries.Point
    {
        const int GoogleMapSRID = 4326;

        public GeoLocation(double longitude, double latitude)
            : base(x: longitude, y: latitude) =>
              base.SRID = GoogleMapSRID;

  
        public double Longitude => base.X;

        public double Latitude => base.Y;


       
    }
}
