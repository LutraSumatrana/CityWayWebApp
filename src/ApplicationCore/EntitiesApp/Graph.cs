﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.EntitiesApp
{
    public class StopsGraph //граф зупинок
    {
        public Dictionary<int, int[]> Edges;  //id вершини та перелік суміжних

        public StopsGraph()
        {
            Edges = new Dictionary<int, int[]>();
        }

        public int[] Adjacent(int id)
        {
            return Edges[id];
        }


  
    }
}
