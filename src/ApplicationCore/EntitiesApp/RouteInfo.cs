﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.EntitiesApp
{
    public class RouteInfo
    {
        public string TransportTypeId { get; set; }
        public string RouteName { get; set; }
        public string RouteNameExtention { get; set; }
        public int RouteTerminalStart { get; set; }
        public int RouteTerminalFinish { get; set; }
        public decimal? RouteFare { get; set; }
        public TimeSpan? RouteTimespan { get; set; } // ??
        public string RouteDescription { get; set; }
        public RouteInfo()
        {

        }

        public List<int> StopPoints { get; set; }  // id усіх точки що проходить маршрут

    }
}
