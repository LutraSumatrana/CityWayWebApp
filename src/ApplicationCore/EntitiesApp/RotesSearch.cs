﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.EntitiesApp
{
    public class RotesSearch
    {
        
        Dictionary<int, int> _From = new Dictionary<int, int>();
        Dictionary<int, double> _Cost = new Dictionary<int, double>();

        StopsGraph _g;
        int _source, _destination; // but it should could be list....

        public RotesSearch(StopsGraph g, int source, int destination) // + criteria
        {
            _g = g;
            _source = source;
            _destination = destination;

        }

     //   public void Search(int _source, int _destination)   if parameters are elevent of list - function would be call in a loop
        public void Search()  //A* search 
        {
           // Queue<int> Spred = new Queue<int>();
           PriorityQueue <int> Spred = new PriorityQueue<int>();


            Spred.Enqueue(_source);
            _From[_source] = _source;
            _Cost[_source] = 0;

            while (Spred.Count() > 0)
            {
                int cur = Spred.Dequeue();
                if (cur == _destination)
                    break;

                foreach (int next in _g.Adjacent(cur))
                {
                    double newCost = _Cost[cur] + 1; //  + cost to pass the edge
                    if (!_Cost.ContainsKey(next) || newCost < _Cost[next])
                    {
                        _Cost[next] = newCost;
                        //double priority = newCost + Func();
                        Spred.Enqueue(next);  // Enqueue(next, priority) !!!
                        _From[next] = cur;
                    }
                }

            }


        }
    }
}
