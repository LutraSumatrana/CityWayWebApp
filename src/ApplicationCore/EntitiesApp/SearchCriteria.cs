﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.EntitiesApp
{
    public class SearchCriteria
    {
        bool Fast { get; set; }        // найкоротший
        bool Cheap { get; set; }        // найдешевший
        bool Optimally { get; set; }   // оптимальний (дешево/швидко)
        bool Directly { get; set; }   // прямий (без пересадок)
    }
}
