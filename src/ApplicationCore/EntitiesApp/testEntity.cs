﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.EntitiesApp
{
    public class testEntity : IComparable<testEntity>
    {
        public string someData;
        public double priority; // меньше значення буде мати більший пріоритет

        public testEntity(string _someData, double priority)
        {
            this.someData = _someData;
            this.priority = priority;
        }
        public override string ToString()
        {
            return "(" + someData + ", " + priority.ToString() + ")";
        }

        public int CompareTo(testEntity other)   // визначаємо метод порівнення
        {
            if (this.priority < other.priority) return -1;
            else if (this.priority > other.priority) return 1;
            else return 0;
        }
    }
}
