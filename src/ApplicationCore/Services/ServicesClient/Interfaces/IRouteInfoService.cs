﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.EntitiesApp;
using NetTopologySuite.Geometries;

namespace ApplicationCore.Services.ServicesClient.Interfaces
{
    public interface IRouteInfoService
    {
        Task<PointInfo> getStopPoint(Geometry location); //отримати найближчу зупинку (location - точка на мапі до якої шукаємо найближчу зупинку)

        Task<RouteInfo> getRouteInfo(int id); //отримати дані про маршрут (id - id маршруту)

        Task<List<RouteInfo>> getRoutesInfoByStopPoint(int id); //отримати дані про маршрути, що проходять через зупинку (id - id точки-зупинки)
        //Task<List<RouteInfo>> getRoutesInfo(PointInfo pointInfo); //отримати дані про маршрути, що проходять через зупинку (pointInfo - зупинка)

        Task<List<RouteInfo>> getRoutesInfo(); //отримати дані про усі маршрути

    }
}
