﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.EntitiesApp;
using NetTopologySuite.Geometries;

namespace ApplicationCore.Services.ServicesClient.Interfaces
{
    interface IRouteSearchService
    {
       
        Task<List<int>> getStopPoints(Geometry location, double proximity); //отримати перелік id зупинок у радіусі proximity (звідки - куди можемо дістатися транспортом)
        Task<List<string>> getAllowedTransport();  // отримуємо з UI перелік типів дтранспорту, що можно використовувати для пошуку маршрутів

        Task<SearchCriteria> getSearchCriteria();  // отримуємо з UI перелік критеріїв пошуку маршрутів

        Task<StopsGraph> buildGraph(List<string> transportCriteria);  // будуємо граф зупинок

        Task<List<Queue<PointInfo>>> searchRoutes(StopsGraph g, int source,  int destination, SearchCriteria searchCriteria); //пошук маршрутів за критеріями
    }
}
