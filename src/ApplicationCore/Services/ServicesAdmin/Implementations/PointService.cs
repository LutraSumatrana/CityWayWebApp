﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using ApplicationCore.Services.ServicesAdmin.Interfaces;
using ApplicationCore.EntitiesApp;
using ApplicationCore.Entities;
using Ardalis.GuardClauses;
using ApplicationCore.Specifications;


namespace ApplicationCore.Services.ServicesAdmin.Implementations
{
    public class PointService : IPointService
    {
        private readonly IAsyncRepository<Point> pointRepository;
        private readonly IAppLogger<PointService> logger;

        public PointService(IAsyncRepository<Point> _pointRepository, IAppLogger<PointService> _logger)
        {
            pointRepository = _pointRepository;
            logger = _logger;
        }

        public PointService(IAsyncRepository<Point> _pointRepository)
        {
            pointRepository = _pointRepository;
        }

        public async Task<int> AddAsync(string _PointName, double _Longitude, double _Latitude)
        {
            Point pointAdd = new()
            {
                PointName = _PointName,
                PointLocation = new GeoLocation(_Longitude, _Latitude)
            };
            // PointByNameLocationSpecification spec = new PointByNameLocationSpecification(_PointName, pointAdd.PointLocation);
            PointByNameLocationSpecification spec = new PointByNameLocationSpecification(_PointName, new GeoLocation(_Longitude, _Latitude));
            Point point = await pointRepository.FirstOrDefaultAsync(spec);
            Guard.Against.DublicatePoint(_PointName, _Longitude, _Latitude, point);
           
            var newPoint = await pointRepository.AddAsync(pointAdd);
            return newPoint.PointId;
        }

        public async Task DeleteAsync(int _PointId)
        {
            Point point = await pointRepository.GetByIdAsync(_PointId);
            Guard.Against.NullPoint(_PointId, point);                       // exception
            //  await pointRepository.DeleteByIdAsync(_PointId);   // 
            await pointRepository.DeleteAsync(point);
        }

        public async Task UpdateAsync(int _PointId, string _PointName, double _Longitude, double _Latitude)
        {
            Point point = await pointRepository.GetByIdAsync(_PointId);
            Guard.Against.NullPoint(_PointId, point);
            point.PointName = _PointName;
            point.PointLocation = new GeoLocation(_Longitude, _Latitude);
            await pointRepository.UpdateAsync(point);
        }
    }
}
