﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ApplicationCore.Services.ServicesAdmin.Interfaces
{
    interface IPointService
    {
        Task <int> AddAsync(string PointName, double Longitude, double Latitude);
        Task DeleteAsync(int PointId);
        Task UpdateAsync(int _PointId, string _PointName, double _Longitude, double _Latitude);
        
        //Get ?
    }
}
