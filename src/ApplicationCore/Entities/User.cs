﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ApplicationCore.Entities
{
    public partial class User : BaseEntity
    {
        public User()
        {
            DataLogs = new HashSet<DataLog>();
        }
        public int UserId { get; set; }
        public string UserLogin { get; set; }
        
        public string UserPassword { get; set; }
        public int AccessLevel { get; set; }

        public virtual ICollection<DataLog> DataLogs { get; set; }
    }
}
