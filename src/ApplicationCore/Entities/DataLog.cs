﻿using System;


namespace ApplicationCore.Entities
{
    public partial class DataLog : BaseEntity
    {
        public int DataLogId { set; get; }
        //  public int UserId { set; get; }
        public int UserId { set; get; }
        public TimeSpan DataTime { get; set; }
        public string Info { get; set; }

        public virtual User User { get; set; }
    }
}
