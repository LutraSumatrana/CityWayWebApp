﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ApplicationCore.Entities
{
    public partial class RoutePoint : BaseEntity
    {
        public int RoutePointId { get; set; }
        public int RouteId { get; set; }
        public int PointId { get; set; }
        public int PointNumber { get; set; }

        //public bool IsBlocked { get; set; }

        public virtual Point Point { get; set; }
        public virtual Route Route { get; set; }
    }
}
