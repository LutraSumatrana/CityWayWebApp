﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ApplicationCore.Entities
{
    public partial class Route : BaseEntity
    {
        public int RouteId { get; set; }
        public string TransportTypeId { get; set; }
        public string RouteName { get; set; }
        public string RouteNameExtention { get; set; }
        public int RouteTerminalStart { get; set; }
        public int RouteTerminalFinish { get; set; }
        public decimal? RouteFare { get; set; }
        public TimeSpan? RouteTimespan { get; set; }
        public string RouteDescription { get; set; }
        //  public bool IsBlocked { get; set; }
        public virtual TransportType TransportType { get; set; }

        // public virtual Point TerminalStart { get; set; }
        //public virtual  Point TerminalFinish { get; set; }

      //  public virtual ICollection<Point> Points { get; set; }
        public virtual ICollection<RoutePoint> RoutePoints { get; set; }
        public virtual ICollection<Timetable> Timetables { get; set; }

        public Route()
        {
            RoutePoints = new HashSet<RoutePoint>();
            Timetables = new HashSet<Timetable>();

           
        }

        //public Route(string _TransportTypeId,
        //                              string _RouteName,
        //                              string _RouteNameExtention,
        //                             // int _RouteTerminalStartId,
        //                             // int _RouteTerminalFinishId,
        //                              decimal _RouteFare,
        //                            //  TimeSpan _RouteTimespan,
        //                              string _RouteDescription)
        //{
        //    TransportTypeId = _TransportTypeId;
        //    RouteName = _RouteName;
        //    RouteNameExtention = _RouteNameExtention;
        //  //  RouteTerminalStart = _RouteTerminalStartId;
        //  //  RouteTerminalFinish = _RouteTerminalFinishId;
        //    RouteFare = _RouteFare;
        //    RouteDescription = _RouteDescription;
        //  //  RouteTimespan = _RouteTimespan;


        //    RoutePoints = new HashSet<RoutePoint>();
        //    Timetables = new HashSet<Timetable>();
        //}

      //  public void AddPoint(int PointId, int Num)
      //  { 
      //       
      //  }

  
    }
}
