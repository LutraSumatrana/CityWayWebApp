﻿using System;


#nullable disable

namespace ApplicationCore.Entities
{
    public partial class Timetable : BaseEntity
    {
        public int TimetableId { get; set; }
        public int RouteId { get; set; }
        public TimeSpan DepartureTime { get; set; }

        public virtual Route Route { get; set; }
    }
}
