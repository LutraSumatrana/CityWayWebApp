﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

#nullable disable

namespace ApplicationCore.Entities
{
  //  public partial class Point : BaseEntity<int>
    public partial class Point : BaseEntity
    {
        public Point()
        {
            RoutePoints = new HashSet<RoutePoint>();
        }
        
        public int PointId { get; set; }
        public string PointName { get; set; }
        public Geometry PointLocation { get; set; }

      //  public bool IsBlocked { get; set; }
        public virtual ICollection<RoutePoint> RoutePoints { get; set; }
    }
}
