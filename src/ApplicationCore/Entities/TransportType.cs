﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ApplicationCore.Entities
{
    public partial class TransportType : BaseEntity
    {
        public TransportType()
        {
            Routes = new HashSet<Route>();
        }

        
        public string TransportTypeId { get; set; }
        public string TransportName { get; set; }

        public virtual ICollection<Route> Routes { get; set; }
    }
}
