﻿using ApplicationCore.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    interface IRoutePointRepository : IAsyncRepository<RoutePoint>
    {
        Task<bool> AddPointAsync(int routeid, int pointId, int num, CancellationToken cancellationToken = default); //додати точку до маршруту

        Task<bool> RemovePointAsync(int routeid, int pointId, int num, CancellationToken cancellationToken = default); //видалити точку з маршруту

        Task<bool> UpdatePointNumAsync(int routeid, int pointId, int num, CancellationToken cancellationToken = default); //оновити точку маршруту
    }
}
