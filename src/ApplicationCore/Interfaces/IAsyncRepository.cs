﻿using System.Collections.Generic;
using System.Threading;  //for CancellationToken structure - propagates notification that operation should be canceled
using System.Threading.Tasks;
using Ardalis.Specification; //base Specification class? for use in creating queries that work whith Repository types
//using Ardalis.Specification.EntityFrameworkCore;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces
{
  

    // public interface IAsyncRepository<T> where T : BaseEntity, IAggregateRoot
    public interface IAsyncRepository<T> where T : BaseEntity
    {
        Task<T> GetByIdAsync(int id, CancellationToken cancellationToken = default);
        Task<IReadOnlyList<T>> ListAllAsync(CancellationToken cancellationToken = default);
        Task<IReadOnlyList<T>> ListAsync(ISpecification<T> spec, CancellationToken cancellationToken = default);
        Task<T> AddAsync(T entity, CancellationToken cancellationToken = default);
        Task UpdateAsync(T entity, CancellationToken cancellationToken = default);
        Task DeleteAsync(T entity, CancellationToken cancellationToken = default);

      //  Task DeleteByIdAsync(int id, CancellationToken cancellationToken = default); //не нужен, если нет записи с id - исключение
        Task<int> CountAsync(ISpecification<T> spec, CancellationToken cancellationToken = default);
        Task<T> FirstAsync(ISpecification<T> spec, CancellationToken cancellationToken = default);
        Task<T> FirstOrDefaultAsync(ISpecification<T> spec, CancellationToken cancellationToken = default);
    }
}
