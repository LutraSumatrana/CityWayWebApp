﻿using ApplicationCore.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ApplicationCore.Interfaces
{
    interface IDataLogRepository :IAsyncRepository<DataLog>
    {

    }


}
