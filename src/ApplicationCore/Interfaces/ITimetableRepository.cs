﻿using ApplicationCore.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace ApplicationCore.Interfaces
{
    interface ITimetableRepository : IAsyncRepository<Timetable>
    {
        
    }
}
