﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System.Threading;        //CancellationToken
using System.Threading.Tasks;
using System.Collections.Generic;
using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;   // IQueryable

namespace Infrastructure.Data.Repositories
{
    public class EntityRepository<T> : IAsyncRepository<T> where T : BaseEntity
    {
        protected readonly CityWayDBContext dbContext;

        public EntityRepository(CityWayDBContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public virtual async Task<T> GetByIdAsync(int id, CancellationToken cancellationToken = default)
        {
            var keyValues = new object[] { id };
            return await dbContext.Set<T>().FindAsync(keyValues, cancellationToken);
        }

        //overload for string type Id - tested
        public virtual async Task<T> GetByIdAsync(string id, CancellationToken cancellationToken = default)
        {
            var keyValues = new object[] { id };
            return await dbContext.Set<T>().FindAsync(keyValues, cancellationToken);
        }

        public async Task<IReadOnlyList<T>> ListAllAsync(CancellationToken cancellationToken = default)
        {
            return await dbContext.Set<T>().ToListAsync(cancellationToken);
        }

        // tested
        public async Task<IReadOnlyList<T>> ListAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            // var specificationResult = ApplySpecification(spec);
            // return await specificationResult.ToListAsync(cancellationToken);
            // or
             return await ApplySpecification(spec).ToListAsync(cancellationToken);
        }

        //tested
        public async Task<int> CountAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            //var specificationResult = ApplySpecification(spec);
            //return await specificationResult.CountAsync(cancellationToken);
            //or
            return await ApplySpecification(spec).CountAsync(cancellationToken);
        }

          // додання запису - tested
          public async Task<T> AddAsync(T entity, CancellationToken cancellationToken = default)
          {
              await dbContext.Set<T>().AddAsync(entity);
              await dbContext.SaveChangesAsync(cancellationToken);

              return entity;
          }

      /*  public Task<T> AddAsync(T item)
        {
            var entity = dbContext.Set<T>().Add(item);
            int i = dbContext.SaveChanges();
            return Task.FromResult(entity.Entity);
        }*/


        public async Task UpdateAsync(T entity, CancellationToken cancellationToken = default)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
            await dbContext.SaveChangesAsync(cancellationToken);
        }

        // видалення запису - tested
        public async Task DeleteAsync(T entity, CancellationToken cancellationToken = default)  
        {
            dbContext.Set<T>().Remove(entity);
            await dbContext.SaveChangesAsync(cancellationToken);
        }

        // видалелення запису за int Id
        //public async Task DeleteByIdAsync(int id, CancellationToken cancellationToken = default)  
        //{
        //    var keyValues = new object[] { id };
        //    T entity  = await dbContext.Set<T>().FindAsync(keyValues, cancellationToken);
        //    dbContext.Set<T>().Remove(entity);                                                  // перевірка наявністі запису с id ???
        //    await dbContext.SaveChangesAsync(cancellationToken);
        //}

        // видалелення запису за string Id - tested
        public async Task DeleteByIdAsync(string id, CancellationToken cancellationToken = default)  
        {
            var keyValues = new object[] { id };
            T entity = await dbContext.Set<T>().FindAsync(keyValues, cancellationToken);
            dbContext.Set<T>().Remove(entity);
            await dbContext.SaveChangesAsync(cancellationToken);
        }

        // tested
        public async Task<T> FirstAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            //var specificationResult = ApplySpecification(spec);
            //return await specificationResult.FirstAsync(cancellationToken);
            //or
            return await ApplySpecification(spec).FirstAsync(cancellationToken);
        }

        // tested
        public async Task<T> FirstOrDefaultAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            //var specificationResult = ApplySpecification(spec);
            //return await specificationResult.FirstOrDefaultAsync(cancellationToken);
            //or
            return await ApplySpecification(spec).FirstOrDefaultAsync(cancellationToken);
        }

        private IQueryable<T> ApplySpecification(ISpecification<T> spec)
        {
            // var evaluator = new SpecificationEvaluator<T>(); //non-generic type 'specification evaluator' cannot be used with type arguments

            // var evaluator = new SpecificationEvaluator();
            // return evaluator.GetQuery(dbContext.Set<T>().AsQueryable(), spec);

            //  or
       
            return SpecificationEvaluator.Default.GetQuery(dbContext.Set<T>().AsQueryable(), spec);
        }
    }
}
