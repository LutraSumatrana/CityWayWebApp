﻿using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using System.Reflection;  //for modelBuilder.ApplyConfigurationsFromAssembly(Assembly.
#nullable disable

namespace Infrastructure.Data
{
    public partial class CityWayDBContext : DbContext
    {
        public CityWayDBContext()
        {
        }

        public CityWayDBContext(DbContextOptions<CityWayDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Point> Points { get; set; }
        public virtual DbSet<Route> Routes { get; set; }
        public virtual DbSet<RoutePoint> RoutePoints { get; set; }
        public virtual DbSet<Timetable> Timetables { get; set; }
        public virtual DbSet<TransportType> TransportTypes { get; set; }
        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<DataLog> DataLogsUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=.\\sqlexpress;Database=CityWay;Trusted_Connection=True;", x => x.UseNetTopologySuite());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            //modelBuilder.Entity<Point>(entity =>
            //{
            //    entity.Property(e => e.PointLocation).IsRequired();

            //    entity.Property(e => e.PointName).HasMaxLength(30);
            //});

            //modelBuilder.Entity<Route>(entity =>
            //{
            //    entity.Property(e => e.RouteDescription).HasMaxLength(200);

            //    entity.Property(e => e.RouteFare)
            //        .HasColumnType("smallmoney")
            //        .HasDefaultValueSql("((0))");

            //    entity.Property(e => e.RouteName)
            //        .IsRequired()
            //        .HasMaxLength(15);

            //    entity.Property(e => e.RouteNameExtention).HasMaxLength(10);

            //    entity.Property(e => e.RouteTimespan).HasColumnType("time(0)");

            //    entity.Property(e => e.TransportTypeId)
            //        .IsRequired()
            //        .HasMaxLength(15);

            //    entity.HasOne(d => d.TransportType)
            //        .WithMany(p => p.Routes)
            //        .HasForeignKey(d => d.TransportTypeId)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_Routes_TransportTypes");
            //});

            //modelBuilder.Entity<RoutePoint>(entity =>
            //{
            //    entity.HasOne(d => d.Point)
            //        .WithMany(p => p.RoutePoints)
            //        .HasForeignKey(d => d.PointId)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_RoutePoints_Points");

            //    entity.HasOne(d => d.Route)
            //        .WithMany(p => p.RoutePoints)
            //        .HasForeignKey(d => d.RouteId)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_RoutePoints_Routes");
            //});

            //modelBuilder.Entity<Timetable>(entity =>
            //{
            //    entity.ToTable("Timetable");

            //    entity.Property(e => e.DepartureTime).HasColumnType("time(0)");

            //    entity.HasOne(d => d.Route)
            //        .WithMany(p => p.Timetables)
            //        .HasForeignKey(d => d.RouteId)
            //        .OnDelete(DeleteBehavior.ClientSetNull)
            //        .HasConstraintName("FK_Timetable_Routes");
            //});

            //modelBuilder.Entity<TransportType>(entity =>
            //{
            //    entity.Property(e => e.TransportTypeId).HasMaxLength(15);

            //    entity.Property(e => e.TransportName)
            //        .IsRequired()
            //        .HasMaxLength(50);
            //});

            //modelBuilder.Entity<User>(entity =>
            //{
            //    entity.Property(e => e.UserLogin)
            //        .IsRequired()
            //        .HasMaxLength(20);

            //    entity.Property(e => e.UserPassword)
            //        .IsRequired()
            //        .HasMaxLength(30);
            //});

            //modelBuilder.Entity<DataLog>(entity =>
            //{
            //    entity.ToTable("DataLogs");
            //    entity.Property(e => e.DataTime)
            //         .IsRequired()
            //         .HasColumnType("time(0)");

            //    entity.Property(e => e.Info)
            //        .IsRequired()
            //        .HasMaxLength(300);

            //    entity.HasOne(d => d.User)
            //       .WithMany(p => p.DataLogs)
            //       .HasForeignKey(d => d.UserId)
            //       .OnDelete(DeleteBehavior.ClientSetNull)
            //       .HasConstraintName("FK_DataLogs_Users");
            //});

            // OnModelCreatingPartial(modelBuilder);
        }

      //  partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
