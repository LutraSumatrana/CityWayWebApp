﻿using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Configurations
{
 
    public class RoutePointsConfiguration : IEntityTypeConfiguration<RoutePoint>
    {
        public void Configure(EntityTypeBuilder<RoutePoint> builder)
        {
            builder.ToTable("RoutePoints");

            builder.HasOne(d => d.Point)
                    .WithMany(p => p.RoutePoints)
                 // .WithMany()
                    .HasForeignKey(d => d.PointId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoutePoints_Points");

            builder.HasOne(d => d.Route)
                     .WithMany(p => p.RoutePoints)
                   //.WithMany()
                    .HasForeignKey(d => d.RouteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoutePoints_Routes");
          
        }
    }


}
