﻿using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Configurations
{
   
    public class TimetableConfiguration : IEntityTypeConfiguration<Timetable>
    {
        public void Configure(EntityTypeBuilder<Timetable> builder)
        {
            builder.ToTable("Timetable");

            builder.Property(e => e.DepartureTime).HasColumnType("time(0)");

            builder.HasOne(d => d.Route)
                            .WithMany(p => p.Timetables)
                            .HasForeignKey(d => d.RouteId)
                            .OnDelete(DeleteBehavior.ClientSetNull)
                            .HasConstraintName("FK_Timetable_Routes");


        }
    }

}
