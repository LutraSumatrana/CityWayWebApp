﻿using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Configurations
{
    public class DataLogConfiguration : IEntityTypeConfiguration<DataLog>
    {
        public void Configure(EntityTypeBuilder<DataLog> builder)
        {
            

            builder.ToTable("DataLogs");
            builder.Property(e => e.DataTime)
                 .IsRequired()
                 .HasColumnType("time(0)");

            builder.Property(e => e.Info)
                .IsRequired()
                .HasMaxLength(300);

            builder.HasOne(d => d.User)
               .WithMany(p => p.DataLogs)
               .HasForeignKey(d => d.UserId)
               .OnDelete(DeleteBehavior.ClientSetNull)
               .HasConstraintName("FK_DataLogs_Users");


        }
    }
}
