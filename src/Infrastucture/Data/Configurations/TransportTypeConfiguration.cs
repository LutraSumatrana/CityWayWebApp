﻿using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Infrastructure.Data.Configurations
{

    public class TransportTypeConfiguration : IEntityTypeConfiguration<TransportType>
    {
        public void Configure(EntityTypeBuilder<TransportType> builder)
        {
            builder.ToTable("TransportTypes");

            builder.Property(e => e.TransportTypeId).HasMaxLength(15);

            builder.Property(e => e.TransportName)
                    .IsRequired()
                    .HasMaxLength(50);

        }
    }
}
