﻿using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Infrastructure.Data.Configurations
{

    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.Property(e => e.UserLogin)
                    .IsRequired()
                    .HasMaxLength(20);


            builder.Property(e => e.UserPassword)
                .IsRequired()
                .HasMaxLength(30);


        }
    }
}
