﻿using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Infrastructure.Data.Configurations
{
    public class RouteConfiguration : IEntityTypeConfiguration<Route>
    {
        public void Configure(EntityTypeBuilder<Route> builder)
        {
            builder.ToTable("Routes");

            builder.Property(e => e.RouteDescription).HasMaxLength(200);

                builder.Property(e => e.RouteFare)
                    .HasColumnType("smallmoney")
                    .HasDefaultValueSql("((0))");

                builder.Property(e => e.RouteName)
                    .IsRequired()
                    .HasMaxLength(15);

                builder.Property(e => e.RouteNameExtention).HasMaxLength(10);

                builder.Property(e => e.RouteTimespan).HasColumnType("time(0)");

                builder.Property(e => e.TransportTypeId)
                    .IsRequired()
                    .HasMaxLength(15);

            builder.HasOne(d => d.TransportType)
            //.WithMany() ???
            .WithMany(p => p.Routes)
            .HasForeignKey(d => d.TransportTypeId)
            .OnDelete(DeleteBehavior.ClientSetNull)
            .HasConstraintName("FK_Routes_TransportTypes");



        }
    }
}
