﻿using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;



//useng Microsoft

namespace Infrastructure.Data.Configurations
{
    public class PointConfiguration : IEntityTypeConfiguration<Point>
    {
        public void Configure(EntityTypeBuilder<Point> builder)
        {
            builder.ToTable("Points");

            builder.Property(e => e.PointLocation).IsRequired();

            builder.Property(e => e.PointName).HasMaxLength(30);
            
       
        }
    }
}
