﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure.Data;

namespace Web.Pages.Points
{
    public class DeleteModel : PageModel
    {
        private readonly Infrastructure.Data.CityWayDBContext _context;

        public DeleteModel(Infrastructure.Data.CityWayDBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Point Point { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Point = await _context.Points.FirstOrDefaultAsync(m => m.PointId == id);

            if (Point == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Point = await _context.Points.FindAsync(id);

            if (Point != null)
            {
                _context.Points.Remove(Point);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
