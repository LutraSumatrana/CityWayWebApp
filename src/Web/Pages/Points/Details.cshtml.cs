﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure.Data;

namespace Web.Pages.Points
{
    public class DetailsModel : PageModel
    {
        private readonly Infrastructure.Data.CityWayDBContext _context;

        public DetailsModel(Infrastructure.Data.CityWayDBContext context)
        {
            _context = context;
        }

        public Point Point { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Point = await _context.Points.FirstOrDefaultAsync(m => m.PointId == id);

            if (Point == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
