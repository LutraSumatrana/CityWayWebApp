﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure.Data;

namespace Web.Pages.Timetable
{
    public class IndexModel : PageModel
    {
        private readonly Infrastructure.Data.CityWayDBContext _context;

        public IndexModel(Infrastructure.Data.CityWayDBContext context)
        {
            _context = context;
        }

        public IList<ApplicationCore.Entities.Timetable> Timetable { get;set; }

        public async Task OnGetAsync()
        {
            Timetable = await _context.Timetables
                .Include(t => t.Route).ToListAsync();
        }
    }
}
