﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure.Data;

namespace Web.Pages.Timetable
{
    public class DetailsModel : PageModel
    {
        private readonly Infrastructure.Data.CityWayDBContext _context;

        public DetailsModel(Infrastructure.Data.CityWayDBContext context)
        {
            _context = context;
        }

        public ApplicationCore.Entities.Timetable Timetable { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Timetable = await _context.Timetables
                .Include(t => t.Route).FirstOrDefaultAsync(m => m.TimetableId == id);

            if (Timetable == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
