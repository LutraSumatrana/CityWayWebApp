﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure.Data;

namespace Web.Pages.Timetable
{
    public class EditModel : PageModel
    {
        private readonly Infrastructure.Data.CityWayDBContext _context;

        public EditModel(Infrastructure.Data.CityWayDBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ApplicationCore.Entities.Timetable Timetable { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Timetable = await _context.Timetables
                .Include(t => t.Route).FirstOrDefaultAsync(m => m.TimetableId == id);

            if (Timetable == null)
            {
                return NotFound();
            }
           ViewData["RouteId"] = new SelectList(_context.Routes, "RouteId", "RouteName");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Timetable).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TimetableExists(Timetable.TimetableId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TimetableExists(int id)
        {
            return _context.Timetables.Any(e => e.TimetableId == id);
        }
    }
}
