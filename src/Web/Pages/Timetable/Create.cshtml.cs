﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ApplicationCore.Entities;
using Infrastructure.Data;

namespace Web.Pages.Timetable
{
    public class CreateModel : PageModel
    {
        private readonly Infrastructure.Data.CityWayDBContext _context;

        public CreateModel(Infrastructure.Data.CityWayDBContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["RouteId"] = new SelectList(_context.Routes, "RouteId", "RouteName");
            return Page();
        }

        [BindProperty]
        public ApplicationCore.Entities.Timetable Timetable { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Timetables.Add(Timetable);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
