﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ApplicationCore.Entities;
using Infrastructure.Data;

namespace Web.Pages.Routes
{
    public class CreateModel : PageModel
    {
        private readonly Infrastructure.Data.CityWayDBContext _context;

        public CreateModel(Infrastructure.Data.CityWayDBContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["TransportTypeId"] = new SelectList(_context.TransportTypes, "TransportTypeId", "TransportTypeId");
            return Page();
        }

        [BindProperty]
        public Route Route { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Routes.Add(Route);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
