﻿using ApplicationCore.Interfaces;
using ApplicationCore.Services;
using Infrastructure.Data;
using Infrastructure.Data.Repositories;
//using Infrastructure.Logging;
//using Infrastructure.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


using ApplicationCore.Services.ServicesAdmin.Interfaces;
using ApplicationCore.Services.ServicesAdmin.Implementations;

//using ApplicationCore.Services.ServicesClient.Interfaces;
//using ApplicationCore.Services.ServicesClient.Implementations;


namespace Web.Configuration
{
    public static class ConfigureCoreServices
    {
        public static IServiceCollection AddCoreServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(typeof(IAsyncRepository<>), typeof(EntityRepository<>));

            services.AddScoped<IRouteService, RouteService>();

          //  services.AddScoped<IRouteInfoService, RouteInfoService>();
          //  services.AddScoped<IRouteSearchService, RouteSearchService>();

            return services;
        }
       
    }
}
