using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Data;

using Microsoft.EntityFrameworkCore;

using Web.Configuration;


namespace Web
{
    //����� ���������� ������������ ����������, ����������� �������, ������� ���������� ����� ������������,
    //������������� ���������� ��� ��������� ������� ��� middleware.

    //����� Startup ������ ���������� ����� Configure()
    //����������� � Startup ����� ���������� ����������� ������ � ����� ConfigureServices().

    //��� ������� ���������� ������� ����������� �����������, ����� ����� ConfigureServices() � � ����� ����� Configure()
    //������ ���������� ������ ���������� ASP.NET.
    public class Startup
    {

       // private IServiceCollection _services;
        public IConfiguration Configuration { get; }
        // IWebHostEnvironment _env;  // IConfiguration,  IWebHostEnvironment - �������� ��� ���������� �� ���������

        public Startup(IConfiguration configuration) //IConfiguration �����������z ����������
        {
            Configuration = configuration;
        }

        public void ConfigureProductionServices(IServiceCollection services)
        {
            // use real database
            // Requires LocalDB which can be installed with SQL Server Express 2016
            // https://www.microsoft.com/en-us/download/details.aspx?id=54284
            //services.AddDbContext<CityWayDBContext>(c =>
            //    //  c.UseSqlServer(Configuration.GetConnectionString("CityWayConnection")));
            //    c.UseSqlServer(Configuration.GetConnectionString("CityWayConnectionString")));

            //// Add Identity DbContext
            //services.AddDbContext<AppIdentityDbContext>(options =>
            //    options.UseSqlServer(Configuration.GetConnectionString("IdentityConnection")));

            ConfigureServices(services);
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) //������������ �������, ������� ������������ �����������
                                                                   //��������� ������ IServiceCollection - ��������� �������� � ����������
        {
            services.AddDbContext<CityWayDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("CityWayConnectionString")));
            services.AddRazorPages();
            //services.AddRazorPages(options =>
            //{
            //    options.Conventions.AuthorizePage("/Basket/Checkout");

            //});

            // services.AddCoreServices(Configuration);
            //services.AddWebServices(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)  //�������������, ��� ���������� ����� ������������ ������.
                                                                                 //IApplicationBuilder l�� ��������� �����������, ������� ������������ ������
                                                                                 //IWebHostEnvironment ��� ���������� � �����, � ������� ����������� ����������, � �������������� � ���.
        {
            if (env.IsDevelopment())  // ���� ���������� � �������� ����������
            {
                app.UseDeveloperExceptionPage();  // �� ������� ��������� ��������� �� ������� 
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

           // app.UseHttpsRedirection();  //�������������� ��� ������� HTTP �� HTTPS
           // app.UseStaticFiles();       //������������� ��������� ��������� ����������� ������

            app.UseRouting();  //EndpointRoutingMiddleware   // ��������� ����������� �������������, ���������� ������ ���������� ������� � ������������� ����������.

            app.UseAuthorization();  //������������� ��������� ��������������

            app.UseEndpoints(endpoints =>  //EndpointMiddleware   // ������������� ������, ������� ����� ��������������
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
